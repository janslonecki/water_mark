$(document).ready(function() {
imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
waterMarkLoader = document.getElementById('waterMarkLoader');
    waterMarkLoader.addEventListener('change', handleWaterMark, false);
preview = document.getElementById('imagePreview');
previewctx = preview.getContext('2d');
canvas = document.getElementById('imageCanvas');
ctx = canvas.getContext('2d');
img = new Image();
watermark = new Image();
wrapper = $('#preview-wrapper');


$(window).resize(function(){
	width = wrapper.width();
	$('#size').text(width);
});


function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        img.onload = function(){
        	$('#preview-wrapper').show();

            var imgratio = (img.width / img.height);

            if (imgratio > 1.4) {
                wrapper.width('100%');
                wrapper.height(wrapper.width() / imgratio);        	
            } else if (imgratio < 1.4 && imgratio > 1.1) {
                wrapper.width('75%');
                wrapper.height(wrapper.width() / imgratio); 
            } else {
                wrapper.width('50%');
                wrapper.height(wrapper.width() / imgratio); 
            };
            

            preview.height = wrapper.height();
            preview.width = wrapper.width();
            previewctx.drawImage(img,0,0, preview.width, preview.height);


				$(window).resize(function(){

            if (imgratio > 1) {
                wrapper.width('100%');
                wrapper.height(wrapper.width() / imgratio);        	
            } else {
                wrapper.width('75%');
                wrapper.height(wrapper.width() / imgratio); 
            };

		            preview.height = wrapper.height();
		            preview.width = wrapper.width();
		            previewctx.drawImage(img,0,0, preview.width, preview.height);
				});

	     	canvas.width = img.width;
	    	canvas.height = img.height;
	        ctx.drawImage(img,0,0,canvas.width,canvas.height);

			function setTextSettings(){
				var fontweight = 'normal';
				var fontsize = 80+'px';
				var fontstyle = 'Calibri';
				
				return {
					size: fontsize,
					weight: fontweight,
					style: fontstyle
				};
			};

			var textset = setTextSettings();
			var textsettings = (textset.weight+' '+textset.size+' '+textset.style);

    	$('#watermarktext').bind('input', function(){
		    canvas.width = img.width;
	    	canvas.height = img.height;
	        ctx.drawImage(img,0,0,canvas.width,canvas.height);              
    	    previewctx.drawImage(img,0,0, preview.width, preview.height);
    		content = $(this).val();

    		
    		

    		previewctx.font = textsettings;
    		previewctx.fillStyle = 'black';
    		previewctx.strokeStyle = 'white';
    		previewctx.lineWidth = 4;
            previewctx.strokeText(content, 100, 100);
    		previewctx.fillText(content,100,100);


    	});

        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);

};




function handleWaterMark(e){

    var wmreader = new FileReader();

    wmreader.onload = function(event){

    	$('#imagePreview').addClass('wmloaded');
    	$('.point-reminder').show();

        function getCursorImg(event){
           var cursor = event.target.result;
           return {cursor: cursor};
        };
        var cursorimg = getCursorImg(event);
        var cursor = cursorimg.cursor
	  	
		function getCursorSize(preview, img){
	        var cursor = event.target.result;
	        var cursorratio = (img.width / preview.width);
	        var wmwidth = watermark.width;
	        var wmheight = watermark.height;
	        return {
	        	width: (wmwidth / cursorratio),
	        	height: (wmheight / cursorratio)
	        };
		};




function getPreviewPos(preview, evt) {
var rect = preview.getBoundingClientRect();
return {
x: evt.clientX - rect.left,
y: evt.clientY - rect.top
};
};

function getMousePos(evt) {
    var mousePos = getPreviewPos(preview, evt);
    return {
    	xpos: mousePos.x,
        ypos: mousePos.y
    };
};

	  	$('#cursor img').attr('src', ''+cursor+'');

        var cursorsize = getCursorSize(preview, img);


	  	$('#cursor img').css('width', ''+cursorsize.width+'px');
	  	$('#cursor img').css('height', ''+cursorsize.height+'px');

	    $('#imagePreview').bind('mouseleave', function(){
	    	$('#cursor').hide();
	    });
	    $('#imagePreview').bind('mouseenter', function(){
	    	$('#cursor').show();
	    })

		  	preview.addEventListener('mousemove', function(e){
		  	var	windowwidth = $(window).width();
			  	if (windowwidth < 1024) {
			    $('#cursor').css({
			      left: e.pageX - (cursorsize.width * 0.5)
			     ,top:  e.pageY - (cursorsize.height * 0.5),
			    });} else {
				  	var toolswidth = $('#tools').width();
				    $('#cursor').css({
				      left: (e.pageX - toolswidth - 30 - (cursorsize.width * 0.5))
				     ,top:  e.pageY - (cursorsize.height * 0.5),
				    });
			    };
			});

	    $(window).resize(function(){
            var cursorimg = getCursorImg(event);
		    var cursor = cursorimg.cursor;
		  	
		  	$('#cursor img').attr('src', ''+cursor+'');
            
		  	var cursorsize = getCursorSize(preview, img);

		  	$('#cursor img').css('width', ''+cursorsize.width+'px');
		  	$('#cursor img').css('height', ''+cursorsize.height+'px');

		    $('#imagePreview').bind('mouseleave', function(){
		    	$('#cursor').hide();
		    });
		    $('#imagePreview').bind('mouseenter', function(){
		    	$('#cursor').show();
		    })
			

		  	preview.addEventListener('mousemove', function(e){
		  	var	windowwidth = $(window).width();
			  	if (windowwidth < 1024) {
			    $('#cursor').css({
			      left: e.pageX - (cursorsize.width * 0.5)
			     ,top:  e.pageY - (cursorsize.height * 0.5),
			    });} else {
				  	var toolswidth = $('#tools').width();
				    $('#cursor').css({
				      left: (e.pageX - toolswidth - 30 - (cursorsize.width * 0.5))
				     ,top:  e.pageY - (cursorsize.height * 0.5),
				    });
			    };
			});
	    });

	    preview.addEventListener('click', function(evt) {

		    var xratio = (img.width / preview.width);
		    var yratio = (img.height / preview.height);
		    var watermarkpreviewwidth = (watermark.width / xratio);
		    var watermarkpreviewheight = (watermark.height / yratio);

		    var mousepos = getMousePos(evt)

		    var prevxpos = (mousepos.xpos - (watermarkpreviewwidth * 0.5));
		    var prevypos = (mousepos.ypos - (watermarkpreviewheight * 0.5));

		    function setMousePos(evt){
			    var mousePos = getPreviewPos(preview, evt);
			    return {
			    	xpos: mousePos.x,
			        ypos: mousePos.y
			    };
		    };

		    function setOgWidth(){
                var previewwidth = $('#imagePreview').width();
                return {
                	ogwidth: previewwidth
                };
		    };
            og = setOgWidth();
            var captmouse = setMousePos(evt);
            captxpos = captmouse.xpos;
            captypos = captmouse.ypos;

		    previewctx.clearRect(0, 0, preview.width, preview.height);

		    previewctx.drawImage(img,0,0, preview.width, preview.height);
		    previewctx.drawImage(watermark,prevxpos,prevypos, watermarkpreviewwidth, watermarkpreviewheight);

		  	canvas.width = img.width;
		  	canvas.height = img.height;

            var finalxpos = (prevxpos * xratio);
            var finalypos = (prevypos * yratio);

		    ctx.drawImage(img,0,0,canvas.width,canvas.height);
		    ctx.drawImage(watermark,finalxpos,finalypos);

	    }, false);

	    if (preview.width < 1130) {
		    $(window).resize(function(){
		    	var ratio = (og.ogwidth / preview.width);
			    var xratio = (img.width / preview.width);
			    var yratio = (img.height / preview.height);
			    var watermarkpreviewwidth = (watermark.width / xratio);
			    var watermarkpreviewheight = (watermark.height / yratio);


                var postxpos = ((captxpos / ratio) - (watermarkpreviewwidth * 0.5));
		        var postypos = ((captypos / ratio) - (watermarkpreviewheight * 0.5));

			    previewctx.clearRect(0, 0, preview.width, preview.height);

			    previewctx.drawImage(img,0,0, preview.width, preview.height);
			    previewctx.drawImage(watermark,postxpos,postypos, watermarkpreviewwidth, watermarkpreviewheight);			    	
		    });		    	
		};

        watermark.src = event.target.result;

    };
    wmreader.readAsDataURL(e.target.files[0]);

};


function downloadCanvas(link, imageCanvas, filename) {
    link.href = document.getElementById(imageCanvas).toDataURL('image/jpeg');
    link.download = filename;
};

document.getElementById('download').addEventListener('click', function() {
    downloadCanvas(this, 'imageCanvas', 'tsest.jpg');
}, false);

});